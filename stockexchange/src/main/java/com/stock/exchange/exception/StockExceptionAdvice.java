package com.stock.exchange.exception;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.Errors;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.stock.exchange.model.ErrorMessage;

@RestControllerAdvice
public class StockExceptionAdvice extends ResponseEntityExceptionHandler{
	
	private static final Logger logger = Logger.getLogger(StockExceptionAdvice.class);
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		logger.info(StockExceptionAdvice.class.getName()+" - handleMethodArgumentNotValid() ENTRY");
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setMessage(ex.getMessage());
		errorMessage.setStatusCode(status.value());
		logger.info(StockExceptionAdvice.class.getName()+" - handleMethodArgumentNotValid() EXIT");
		return new ResponseEntity(errorMessage, headers, status);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseEntity<Object> handleHttpMessageNotReadable(
			HttpMessageNotReadableException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		logger.info(StockExceptionAdvice.class.getName()+" - handleHttpMessageNotReadable() ENTRY");
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setMessage(ex.getMessage());
		errorMessage.setStatusCode(status.value());
		logger.info(StockExceptionAdvice.class.getName()+" - handleHttpMessageNotReadable() EXIT");
		return new ResponseEntity(errorMessage, headers, status);		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseEntity<Object> handleMissingServletRequestParameter(
			MissingServletRequestParameterException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setMessage(ex.getMessage());
		errorMessage.setStatusCode(status.value());
		return new ResponseEntity(errorMessage, headers, status);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseEntity<Object> handleHttpRequestMethodNotSupported(
			HttpRequestMethodNotSupportedException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setMessage(ex.getMessage());
		errorMessage.setStatusCode(status.value());
		return new ResponseEntity(errorMessage, headers, status);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ResponseEntity<Object> handleServletRequestBindingException(
			ServletRequestBindingException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setMessage(ex.getMessage());
		errorMessage.setStatusCode(status.value());
		return new ResponseEntity(errorMessage, headers, status);
	}
	
	@ExceptionHandler
    @ResponseBody
	public ResponseEntity<?> handleException(IllegalArgumentException exception) {
				
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setMessage(exception.getMessage());
		errorMessage.setStatusCode(HttpStatus.BAD_REQUEST.value());
		
		return new ResponseEntity<ErrorMessage>(errorMessage, HttpStatus.BAD_REQUEST);
	}
	
	
	@ExceptionHandler	
    @ResponseBody
	public ResponseEntity<?> handleException(Exception exception) {
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setMessage(exception.getMessage());
		errorMessage.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		
		return new ResponseEntity<ErrorMessage>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
}
