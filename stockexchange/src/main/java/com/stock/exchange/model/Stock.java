package com.stock.exchange.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
@Entity
@Table(name="Stock")
public class Stock {
	@Id
	private Integer stockid;
	private String stockname;
	private Double stockpriceini;
	private Double stockpricerev;
	private Integer stocknum;
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern="dd:MM:yyyy hh:mm a",timezone="IST")
	private Date createdon;
	
	public Integer getStockid() {
		return stockid;
	}
	public void setStockid(Integer stockid) {
		this.stockid = stockid;
	}
	public String getStockname() {
		return stockname;
	}
	public void setStockname(String stockname) {
		this.stockname = stockname;
	}
	public Double getStockpriceini() {
		return stockpriceini;
	}
	public void setStockpriceini(Double stockpriceini) {
		this.stockpriceini = stockpriceini;
	}
	public Double getStockpricerev() {
		return stockpricerev;
	}
	public void setStockpricerev(Double stockpricerev) {
		this.stockpricerev = stockpricerev;
	}
	public Integer getStocknum() {
		return stocknum;
	}
	public void setStocknum(Integer stocknum) {
		this.stocknum = stocknum;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}	
}
