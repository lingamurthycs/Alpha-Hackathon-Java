package com.stock.exchange.model;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StockConfirm {
	@NotEmpty(message="name should not be empty")
	private String name;
	@JsonProperty(value="stockName")
	private String stockname;
	@JsonProperty(value="noOfStock")
	private Integer noofstock;
	public String getStockname() {
		return stockname;
	}
	public void setStockname(String stockname) {
		this.stockname = stockname;
	}
	public Integer getNoofstock() {
		return noofstock;
	}
	public void setNoofstock(Integer noofstock) {
		this.noofstock = noofstock;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
