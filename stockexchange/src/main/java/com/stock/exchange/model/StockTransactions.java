package com.stock.exchange.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="stocktxns")
public class StockTransactions {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonProperty("transactionId")
	private Integer txnid;
	@JsonProperty("stockName")
	private String stockname;
	@JsonProperty("volumePurchased")
	private Integer volumepurchased;
	@JsonProperty("stockPrice")
	private Double stockprice;	
	private Double fees;
	@JsonProperty("userName")
	private String user;
	@JsonProperty("transactionDate")
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern="dd-MM-yyyy hh:mm:ss a",timezone="IST")
	private Date txndate;
	@JsonProperty("totalStockPrice")
	private Double totalstockprice;
	@JsonProperty("totalStockPricePlusFees")
	private Double totalstockpriceplusfees;
	public Integer getTxnid() {
		return txnid;
	}
	public void setTxnid(Integer txnid) {
		this.txnid = txnid;
	}
	public String getStockname() {
		return stockname;
	}
	public void setStockname(String stockname) {
		this.stockname = stockname;
	}
	public Integer getVolumepurchased() {
		return volumepurchased;
	}
	public void setVolumepurchased(Integer volumepurchased) {
		this.volumepurchased = volumepurchased;
	}
	public Double getStockprice() {
		return stockprice;
	}
	public void setStockprice(Double stockprice) {
		this.stockprice = stockprice;
	}
	public Double getFees() {
		return fees;
	}
	public void setFees(Double fees) {
		this.fees = fees;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public Date getTxndate() {
		return txndate;
	}
	public void setTxndate(Date txndate) {
		this.txndate = txndate;
	}
	public Double getTotalstockprice() {
		return totalstockprice;
	}
	public void setTotalstockprice(Double totalstockprice) {
		this.totalstockprice = totalstockprice;
	}
	public Double getTotalstockpriceplusfees() {
		return totalstockpriceplusfees;
	}
	public void setTotalstockpriceplusfees(Double totalstockpriceplusfees) {
		this.totalstockpriceplusfees = totalstockpriceplusfees;
	}	
}
