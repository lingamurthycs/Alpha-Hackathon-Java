package com.stock.exchange.model;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StockConfirmDetails {
	@NotNull
	private String stockName;
	@JsonProperty("noOfStock")
	private Integer noofstock;
	private Double price;
	@JsonProperty("borkageCharge")
	private Double borkagecharge;	
	private Double total;
	
	private String name;
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public Integer getNoofstock() {
		return noofstock;
	}
	public void setNoofstock(Integer noofstock) {
		this.noofstock = noofstock;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getBorkagecharge() {
		return borkagecharge;
	}
	public void setBorkagecharge(Double borkagecharge) {
		this.borkagecharge = borkagecharge;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
}
