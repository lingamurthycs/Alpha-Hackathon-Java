package com.stock.exchange.service;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stock.exchange.model.Stock;
import com.stock.exchange.model.StockConfirm;
import com.stock.exchange.model.StockConfirmDetails;
import com.stock.exchange.model.StockTransactions;
import com.stock.exchange.repository.StockRepository;
import com.stock.exchange.repository.StockTransactionsRepository;

@Service
public class StockService {
	
	private static final Logger logger = Logger.getLogger(StockService.class);
	
	@Autowired
	private StockRepository stockRepo;
	
	@Autowired
	private StockTransactionsRepository stockTxnRepo;
	
	public Iterable<Stock> getAllStockDetails(){
		return stockRepo.findAll();
	}
	
	public StockConfirmDetails stockConfirm(StockConfirm stockConfirm){
		logger.info("StockService - stockconfirm - ENTRY");
		StockConfirmDetails scd = new StockConfirmDetails();
		String stockName = stockConfirm.getStockname();
		List<Stock> stockList = stockRepo.findBystockname(stockName);
		for(Stock stock :stockList){
			Integer stocknumber = stock.getStocknum();
			if(stockConfirm.getNoofstock()>stocknumber){
				//error message here
			}else{
				Double stockPrice = stockConfirm.getNoofstock() * stock.getStockpriceini();				
				
				scd.setStockName(stock.getStockname());
				scd.setNoofstock(stockConfirm.getNoofstock());
				scd.setPrice(stockPrice);
				scd.setName(stockConfirm.getName());
				
				// brokarage charges
				if(stockConfirm.getNoofstock()<500){
					Double brokageCharges = stockPrice * (0.1/100);
					scd.setBorkagecharge(brokageCharges);
					Double total = stockPrice + brokageCharges;
					scd.setTotal(total);
				}else if(stockConfirm.getNoofstock()>=500){
					Double brokageCharges = 0.0;
					brokageCharges = 499 * (0.1/100);
					if(stockConfirm.getNoofstock() - 499 < 100){
						brokageCharges = brokageCharges + ((stockConfirm.getNoofstock() - 499) * (0.15/100)); 
					}
					scd.setBorkagecharge(brokageCharges);
					Double total = stockPrice + brokageCharges;
					scd.setTotal(total);
				}
				
			}
		}
		logger.info("StockService - stockconfirm - EXIT");
		return scd;
	}
	
	public StockTransactions stockSubmit(StockConfirmDetails stockConfirmDetails){
		StockTransactions st = new StockTransactions();
		st.setStockname(stockConfirmDetails.getStockName());
		List<Stock> stockList = stockRepo.findBystockname(stockConfirmDetails.getStockName());
		for(Stock stock :stockList){
			st.setStockprice(stock.getStockpricerev());
		}
		st.setFees(stockConfirmDetails.getBorkagecharge());
		st.setTotalstockprice(stockConfirmDetails.getPrice());
		st.setTotalstockpriceplusfees(stockConfirmDetails.getTotal());
		st.setTxndate(new Date());
		st.setUser(stockConfirmDetails.getName());
		st.setVolumepurchased(stockConfirmDetails.getNoofstock());
		stockTxnRepo.save(st);
		return st;
	}
	
	public List<StockTransactions> getTxnsList(String userName){
		return stockTxnRepo.findByuser(userName);
	}

}
