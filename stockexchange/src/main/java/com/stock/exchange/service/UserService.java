package com.stock.exchange.service;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stock.exchange.model.User;
import com.stock.exchange.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepo;
	
	public Iterable<User> getUserDetails(){
		return userRepo.findAll();
	}

}
