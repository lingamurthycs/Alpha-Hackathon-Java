package com.stock.exchange.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.stock.exchange.model.Stock;

public interface StockRepository extends CrudRepository<Stock, Long>{
	List<Stock> findBystockname(String stockname);
}
