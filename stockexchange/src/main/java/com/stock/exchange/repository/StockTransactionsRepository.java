package com.stock.exchange.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.stock.exchange.model.StockTransactions;

public interface StockTransactionsRepository extends CrudRepository<StockTransactions, Long>{
	List<StockTransactions> findByuser(String user);
}
