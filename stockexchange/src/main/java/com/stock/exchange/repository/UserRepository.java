package com.stock.exchange.repository;

import org.springframework.data.repository.CrudRepository;

import com.stock.exchange.model.User;

public interface UserRepository extends CrudRepository<User, Long>{

}
