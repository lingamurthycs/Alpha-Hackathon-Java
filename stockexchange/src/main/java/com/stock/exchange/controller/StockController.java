package com.stock.exchange.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.qos.logback.classic.Logger;

import com.stock.exchange.model.Stock;
import com.stock.exchange.model.StockConfirm;
import com.stock.exchange.model.StockConfirmDetails;
import com.stock.exchange.model.StockTransactions;
import com.stock.exchange.service.StockService;

@CrossOrigin("*")
@RestController
public class StockController {
	
	@Autowired
	private StockService stockService;
	
	@GetMapping(value="/stock/getstockdetails")
	public @ResponseBody Iterable<Stock> getAllStockDetails(){
		return stockService.getAllStockDetails();
	}
	
	@PostMapping(value="/stock/confirm")
	public @ResponseBody StockConfirmDetails stockConfirm(@Valid @RequestBody StockConfirm stockConfirm){		
		return stockService.stockConfirm(stockConfirm);
	}
	
	@PostMapping(value="/stock/submit")
	public @ResponseBody StockTransactions stockSubmit(@Valid @RequestBody StockConfirmDetails stockConfirmDetails){
		return stockService.stockSubmit(stockConfirmDetails);
	}
	
	@GetMapping(value="/stock/list")
	public @ResponseBody List<StockTransactions> getTxnsList(@RequestParam(name="name")String name){
		System.out.println("********name="+name);
		List<StockTransactions> txnsList = stockService.getTxnsList(name);
		/*StockTransactions txn = new StockTransactions();
		txnsList.add(txn);*/
		return txnsList;
	}
}
