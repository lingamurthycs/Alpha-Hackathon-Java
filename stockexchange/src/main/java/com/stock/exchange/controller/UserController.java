package com.stock.exchange.controller;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.stock.exchange.model.User;
import com.stock.exchange.service.UserService;
@CrossOrigin("*")
@RestController
public class UserController {
	@Autowired
	private UserService userService;
	
	@GetMapping(value="/getuserdetails")
	public @ResponseBody Iterable<User> getUserDetails(){
		return userService.getUserDetails();
	}
}
